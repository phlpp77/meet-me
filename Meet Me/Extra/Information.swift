//
//  Information.swift
//  Meet Me
//
//  Created by Philipp Hemkemeyer on 17.01.21.
//

import SwiftUI

// get the size of the screen (defined here that it can be used in every View)
let screen = UIScreen.main.bounds
